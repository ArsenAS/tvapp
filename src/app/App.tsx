import { FC } from "react";
import { HomePage } from "../pages/homePage";
import "./RootStyles.css";
import { Styled } from "./App.styled";

const { Root } = Styled;

export const App: FC = () => {
  return (
    <Root>
      <HomePage />
    </Root>
  );
};
