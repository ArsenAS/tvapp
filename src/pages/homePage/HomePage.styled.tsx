import styled from "styled-components";

const Root = styled.div`
  height: 100%;
  display: flex;
`;

const LeftSide = styled.div`
  width: 100%;
`;

export const Styled = { Root, LeftSide };
