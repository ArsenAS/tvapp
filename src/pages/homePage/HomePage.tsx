import { FC } from "react";
import { SideBar } from "../../components/sideBar";
import { TrendingNow } from "../../components/trendingNow";
import { FilmComponent } from "../../components/filmComponent";
import { Styled } from "./HomePage.styled";

const { Root, LeftSide } = Styled;

export const HomePage: FC = () => {
  const Featured = {
    Id: "1",
    Title: "The Irishman",
    CoverImage: "FeaturedCoverImage.png",
    TitleImage: "FeaturedTitleImage.png",
    Date: "2021-10-24T12:16:50.894556",
    ReleaseYear: "2021",
    MpaRating: "18+",
    Category: "Movie",
    Duration: "6000",
    Description: "Info About it",
  };

  return (
    <Root>
      <SideBar />
      <LeftSide>
        <FilmComponent variant={"mediumFilm"} data={Featured} />
        <TrendingNow />
      </LeftSide>
    </Root>
  );
};
