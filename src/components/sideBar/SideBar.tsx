import { FC } from "react";
import { Styled } from "./SideBar.styled";

const { Root } = Styled;

export const SideBar: FC = () => {
  return <Root>side bar</Root>;
};
