export type Props = {
  variant: "smallFilm" | "mediumFilm" | "fullFilm";
  name?: string;
  data?: any;
};

export type VariantType = {
  smallFilm: JSX.Element;
  mediumFilm: JSX.Element;
  fullFilm: JSX.Element;
};
