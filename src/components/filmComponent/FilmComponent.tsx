import { FC } from "react";
import { SmallFilm } from "./smallFilm";
import { MediumFilm } from "./mediumFilm";
import { FullFilm } from "./fullFilm";
import { Props, VariantType } from "./types";

export const FilmComponent: FC<Props> = ({ variant, name, data }) => {
  const variants: VariantType = {
    smallFilm: <SmallFilm name={name} />,
    mediumFilm: <MediumFilm data={data} />,
    fullFilm: <FullFilm name={name} />,
  };

  return <>{variants[variant]}</>;
};
