import { FC } from "react";
import { Props } from "./types";
import { Styled } from "./MediumFilm.styled";

const { Root, AllInfo, AllInfoButtonContainer } = Styled;

export const MediumFilm: FC<Props> = ({ data }) => {
  const {
    CoverImage,
    Category,
    TitleImage,
    ReleaseYear,
    MpaRating,
    Description,
  } = data;

  const img = require(`../../../common/icons/films/mediumFilm/${CoverImage}`);
  const titleImage = require(`../../../common/icons/films/mediumFilm/${TitleImage}`);

  return (
    <Root img={img}>
      <AllInfo>
        <span>{Category}</span>
        <img src={titleImage} alt={"CoverImage"} />
        <span>
          {ReleaseYear} {MpaRating} 1h 48m
        </span>
        <span>{Description}</span>
        <AllInfoButtonContainer>
          <button>Play</button>
          <button>More Info</button>
        </AllInfoButtonContainer>
      </AllInfo>
    </Root>
  );
};
