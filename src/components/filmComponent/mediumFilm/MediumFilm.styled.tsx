import styled from "styled-components";

type Props = {
  img: string;
};
const Root = styled.div<Props>`
  width: 80%;
  height: 100%;
  background: ${(props) => `url(${props.img})`} no-repeat;
  display: flex;
  align-items: center;
`;

const AllInfo = styled.div`
  width: 500px;
  height: 300px;
  margin-left: 100px;
  display: flex;
  flex-direction: column;
  color: white;
  gap: 10px;
`;
const AllInfoButtonContainer = styled.div`
  display: flex;
`;

export const Styled = { Root, AllInfo, AllInfoButtonContainer };
