import styled from "styled-components";

const Root = styled.div``;
const Img = styled.img`
  cursor: pointer;
`;

export const Styled = { Root, Img };
