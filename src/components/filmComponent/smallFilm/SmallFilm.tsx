import { FC } from "react";
import { Props } from "./types";
import { Styled } from "./SmallFilm.styled";

const { Img } = Styled;

export const SmallFilm: FC<Props> = ({ name }) => {
  const filmImage = require(`/src/common/icons/films/smallFilm/${name}`);
  return <Img alt={name} src={filmImage} />;
};
