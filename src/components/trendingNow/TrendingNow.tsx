import { FC } from "react";
import { FilmComponent } from "../filmComponent";
import AliceCarousel from "react-alice-carousel";
import { Styled } from "./TrendingNow.styled";
const jsonData = require("../../data.json");

const { Root, Title } = Styled;

export const TrendingNow: FC = () => {
  const { TrendingNow } = jsonData;

  const carouselConfig = {
    disableButtonsControls: true,
    mouseTracking: true,
    disableDotsControls: true,
    autoPlay: true,
    infinite: true,
    autoPlayInterval: 3000,
    animationDuration: 1200,
  };

  return (
    <Root>
      <Title>Trending Now</Title>
      <AliceCarousel
        {...carouselConfig}
        responsive={{
          0: {
            items: 1,
          },
          1024: {
            items: 9,
          },
        }}
      >
        {TrendingNow.map((item: any, index: number) => (
          <FilmComponent
            key={index}
            variant={"smallFilm"}
            name={item["CoverImage"]}
          />
        ))}
      </AliceCarousel>
    </Root>
  );
};
