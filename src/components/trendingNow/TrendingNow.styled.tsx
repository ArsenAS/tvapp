import styled from "styled-components";

const Root = styled.div`
  background-color: #040404;
  width: 100%;
`;
const Title = styled.span`
  font-size: 32px;
  color: #f1f1f1;
  margin-bottom: 19px;
  display: block;
`;

export const Styled = { Root, Title };
